Problems with Shivam's OOP solution:

GETTERS AND SETTERS:
- Fields are delcared private but we used getters and setters to access it
- This leads to `leaky abstraction`
- Information should not be let out of the class. Any action/behaviour (calculation in this case) should be done within the class.

LACK OF ENCAPSULATION: 
- State and behaviour were seperated into 2 different classes.
- `Binding of data and behaviour` together was missing.
- Information should not be let out of the class. Any action/behaviour (calculation in this case)should be done within the class.

USE OF STATIC FOR METHODS:
- Here distance/direction is specific to a particular point.
- Static methods are some common methods that is `not dependent on the instance` of that class.
- Can be refactored to instance specific methods. 

UNUSED METHOD:
- The constructor was not used anywhere in the code , used only in test.

NAMING CONVENTION:
- x and y fields (data/ states )can be named as **xcoordinate** and **ycoordinate**.

CAN BE MORE GENERALISED:
- It can made more generic like to accept any number of coordinates and other corner cases. 

#################################################################################
So Shivam had just learned about OOP. He had written a program before that did two things,
- Find distance between two points
- Find direction (angle) between two points in Radians.

He has this code in the `org.procedural.DistanceAndDirectionCalculator` It looks like this for the reference - 

```java
public class DistanceAndDirectionCalculator {
    public static double distance(double x1, double y1, double x2, double y2) {
        double xDistance = x1 - x2;
        double yDistance = y1 - y2;
        return Math.sqrt(Math.pow(xDistance, 2) + Math.pow(yDistance, 2));
    }

    public static double direction(double x1, double y1, double x2, double y2) {
        double xDistance = x2 - x1;
        double yDistance = y2 - y1;
        return Math.atan2(yDistance, xDistance);
    }
}
```
He thought it'll be a good idea to convert this to Object Oriented Programming. So he wrote a new implementation in package `org.oop`, he got 2 classes - 
- `org.oop.DistanceAndDirectionCalculator`
- `org.oop.Point`

However, his trainer told him that what he did is not Object Oriented programming and asked Shivam to try again. 
- Try to articulate problems with Shivam's OOP solution. (Write it somewhere and share it with your trainer)
- Fork the project and fix the design related problem with Shivam's OOP solution. Share that with your trainer too.