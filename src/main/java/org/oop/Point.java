package org.oop;

public class Point {
    private double xCoordinate;
    private double yCoordinate;
    public Point(double xCoordinate, double yCoordinate) {
        this.xCoordinate = xCoordinate;
        this.yCoordinate = yCoordinate;
    }

    public double distance( Point to) {
        double xDistance = to.xCoordinate - this.xCoordinate;
        double yDistance = to.yCoordinate - this.yCoordinate;
        return Math.sqrt(Math.pow(xDistance, 2) + Math.pow(yDistance, 2));
    }

    public double direction(Point to) {
        double xDistance = to.xCoordinate - this.xCoordinate;
        double yDistance = to.yCoordinate - this.yCoordinate;
        return Math.atan2(yDistance, xDistance);
    }
}
